package com.example.demo.repository;

import com.example.demo.entity.TimelineLike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
public interface TimelineLikeRepository extends JpaRepository<TimelineLike, Long> {
}
