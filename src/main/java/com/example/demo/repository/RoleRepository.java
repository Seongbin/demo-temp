package com.example.demo.repository;

import com.example.demo.entity.db.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByuserName(String userName);
    Role save(Role role);
}
