package com.example.demo.entity.db.security;

import lombok.Data;

@Data
public class AuthenticationRequest {
    private String username;
    private String password;
    private String email;
}
