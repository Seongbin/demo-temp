package com.example.demo.entity.db.security;

import lombok.Data;


@Data
public class AuthenticationToken {
    private String token;
}
