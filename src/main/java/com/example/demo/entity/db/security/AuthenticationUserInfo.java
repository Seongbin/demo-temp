package com.example.demo.entity.db.security;

import lombok.Data;

import java.util.Collection;


@Data
public class AuthenticationUserInfo {
//    private String userid;
    private String username;
    private Collection authorities;
}
