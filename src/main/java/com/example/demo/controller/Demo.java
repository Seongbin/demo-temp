package com.example.demo.controller;

import com.example.demo.entity.db.security.AuthenticationRequest;
import com.example.demo.entity.db.security.AuthenticationToken;
import com.example.demo.entity.db.security.AuthenticationUserInfo;
import com.example.demo.security.JwtTokenProvider;
import com.example.demo.service.security.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@RestController
public class Demo {
    private final UserServiceImpl u;

    @Autowired
    private PasswordEncoder p;

    private final JwtTokenProvider jwtTokenProvider;

    //test
    @GetMapping("/me")
    public AuthenticationUserInfo rengsultHelloworld(HttpServletRequest request) {
        String header = request.getHeader("Authorization");
        header = header.substring(7);
        //Bearer가 앞에 붙어서

        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken) jwtTokenProvider.getAuthentication(header);

        AuthenticationUserInfo AuthenticationUserInfo = new AuthenticationUserInfo();
        AuthenticationUserInfo.setUsername(usernamePasswordAuthenticationToken.getName());

        //여기서 유저이름 가지고 검색?

        AuthenticationUserInfo.setAuthorities(usernamePasswordAuthenticationToken.getAuthorities());
//        AuthenticationUserInfo.setUserid(jwtTokenProvider.getUserPk(header));

        return AuthenticationUserInfo;
    }

    // oauth 이용
    // 고객정보
    // 로그인정보 따로 둘 것
    // twillo인가 썼는데 저는
    /*
    19:16 짬뽕/도쿄/웹/1년 숫자 랜덤으로 찍어서 숫자랑 폰번호랑 같이 디비에 저장한다음
    19:16 짬뽕/도쿄/웹/1년 폰으로 그 번호가날라가서
          사용자가 그 번호를 제대로 입력하면
    19:16 짬뽕/도쿄/웹/1년 verify되어서 다음단계로 이동하는

     */
    // https://cusonar.tistory.com/17
    @PostMapping("/login")
    public AuthenticationToken login(@RequestBody AuthenticationRequest user) {

        String password = user.getPassword();
        String username = user.getUsername();

        UserDetails member = u.loadUserByUsername(username);
//            고객정보
        if (!p.matches(password, member.getPassword())) {
            throw new IllegalArgumentException("잘못된 비밀번호입니다.");
        }

        AuthenticationToken authenticationToken = new AuthenticationToken();
        authenticationToken.setToken(jwtTokenProvider.createToken(String.valueOf(user.getUsername())));
        return authenticationToken;
    }

    @PostMapping("/create")
    public String create(@RequestBody AuthenticationRequest createRequestUser) {
        u.createUser(createRequestUser);
        return "success";
    }

}

/*
https://webfirewood.tistory.com/m/115?category=694472

20:25 개발자(이전닉 무직)/3년 6개월 자바에서 제이선형식으로
20:25 개발자(이전닉 무직)/3년 6개월 그냥보내도 되는거죠?
20:25 개발자(이전닉 무직)/3년 6개월 아무리 예제 찾아봐도
20:25 개발자(이전닉 무직)/3년 6개월 이런식으로 토큰발급되었습니다
20:25 개발자(이전닉 무직)/3년 6개월 하고끝내니
20:27 김풀/도쿄/IT/5년 네
20:27 김풀/도쿄/IT/5년 이렇게 하셔도 크게 상관없는거같은데요

 */
