package com.example.demo.controller;

import com.example.demo.entity.TimelineLike;
import com.example.demo.entity.Timeline;
import com.example.demo.service.like.TimelineLikeService;
import com.example.demo.service.timeline.TimelineService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
public class TwitController {

    private final TimelineService timelineService;
    private final TimelineLikeService timelineLikeService;

    //무한스크롤
    @GetMapping("/timeline")
    public Page<Timeline> getTimeline(@RequestParam(value = "pageNumber")  Integer pageNumber){
        return timelineService.findAllbyPageable(pageNumber);
    }

    @PostMapping("/timeline")
    public Timeline uploadTimeline(@RequestBody Timeline timeline) throws Exception{
        if(StringUtils.isEmpty(timeline.getContent())){
            throw new Exception("Please fill content");
        }
        return timelineService.saveAndFlush(timeline);
    }

    @PostMapping("/deleteTimeline")
    public void deleteTimeline(@RequestBody Timeline Timeline){
        timelineService.deleteTimeline(Timeline.getId());
    }

    //insert or update
    //예약어 like 때문에 에러났음
    @PostMapping("/likeTimeline")
    public TimelineLike likeTimeline(@RequestBody TimelineLike timelineLike){
        return timelineLikeService.saveAndFlush(timelineLike);
    }
}