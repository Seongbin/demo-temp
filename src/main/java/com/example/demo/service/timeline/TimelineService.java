package com.example.demo.service.timeline;

import com.example.demo.entity.Timeline;
import com.example.demo.repository.TimelineRepository;
import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
@Transactional
public class TimelineService {

    @Value("${page.size}")
    private Integer pageSize;

    private final TimelineRepository timelineRepository;

    public Page<Timeline> findAllbyPageable(Integer page) {
        Pageable paging = PageRequest.of(page, pageSize, Sort.by("id").descending());
        return timelineRepository.findAll(paging);
    }

    public Timeline saveAndFlush(Timeline timeline) {
        return timelineRepository.saveAndFlush(timeline);
    }

    public void deleteTimeline(Long id) {
        timelineRepository.deleteById(id);
    }
}
