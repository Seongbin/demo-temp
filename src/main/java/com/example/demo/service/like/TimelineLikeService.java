package com.example.demo.service.like;

import com.example.demo.entity.TimelineLike;
import com.example.demo.repository.TimelineLikeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class TimelineLikeService {

    private final TimelineLikeRepository timelineLikeRepository;

    //insert update
    public TimelineLike saveAndFlush(TimelineLike timelineLike) {
        return timelineLikeRepository.saveAndFlush(timelineLike);
    }
}
