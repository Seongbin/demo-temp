package com.example.demo.service.security;

import com.example.demo.entity.db.Role;
import com.example.demo.entity.db.User;
import com.example.demo.entity.db.security.AuthenticationRequest;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.Optional;

@Service
@Slf4j
@Transactional
public class UserServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository u;

    @Autowired
    private RoleRepository r;

    @Autowired
    private  PasswordEncoder e;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //실제 로그인시 동작됨
        return Optional.ofNullable(u.findByuserName(username)).orElseThrow(() -> new UsernameNotFoundException(username));
//        return u.findByuserName(username);
    }

    public User createUser(AuthenticationRequest createRequestUser) {
        //어카운트 만드는것 할것
        String username = createRequestUser.getUsername();
        if (null != u.findByuserName(username)) {
            return null;
        }
        String userPassword = e.encode(createRequestUser.getPassword());
        User user = new User();
        user.setUserName(username);
        user.setPassword(userPassword);
//        user.setRole(Collections.singletonList("ROLE_USER"));// 최초 가입시 USER 로 설정
        user.setRole("1");
        user.setEMail(createRequestUser.getEmail());

        Role role = new Role();
        role.setUserName(username);
        role.setRole(Collections.singletonList("ROLE_USER"));
        role.setRole(null);
        r.save(role);
        return u.save(user);
    }

}
