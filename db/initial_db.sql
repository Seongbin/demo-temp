-- Adminer 4.7.6 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  UNIQUE KEY `ix_auth_username` (`user_name`,`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `timeline`;
CREATE TABLE `timeline` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `content` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `upload_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_name` (`user_name`),
  CONSTRAINT `Timeline_ibfk_1` FOREIGN KEY (`user_name`) REFERENCES `user` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `timeline_like`;
CREATE TABLE `timeline_like` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `timeline_id` bigint NOT NULL,
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `like_flag` tinyint(1) NOT NULL,
  `upload_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `timeline_id` (`timeline_id`),
  KEY `user_name` (`user_name`),
  CONSTRAINT `timeline_like_ibfk_1` FOREIGN KEY (`timeline_id`) REFERENCES `timeline` (`id`),
  CONSTRAINT `timeline_like_ibfk_2` FOREIGN KEY (`user_name`) REFERENCES `user` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(200) NOT NULL,
  `e_mail` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `role` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user` (`user_name`, `password`, `e_mail`, `role`) VALUES
('1',	'{bcrypt}$2a$10$RnF4Z5lSB6fSoLiUbDAMluP08Li7hwhPLWBL9ea6yr563OblYXpXy',	'1@1',	'1'),
('111',	'{bcrypt}$2a$10$vB3Jv1.eaAASEAL3Msq/Z.FQOJX/5bzllCMRImAtxB5QJTCe3m5B6',	'111@111',	'1'),
('2',	'{bcrypt}$2a$10$HSqNCtYXOThBwz4grZexyuCCNBFJUMl3Zrfh0vogkcrmI7eF3PJ7O',	'1@2',	'1'),
('222',	'{bcrypt}$2a$10$sLxRYUBtm/ewW6Wg0p67COWE5bzvscOwStg/U2v/tKVw7.z57V/6K',	'222@222',	'1'),
('333',	'{bcrypt}$2a$10$u.zWC90rrypi0MSOukZxXOENqVaEVJTBSK9svSx2AM2U99FHjdhWm',	'333@333',	'1');

-- 2020-04-29 13:28:44